export default class PerformanceLogger {
    performance = null
    measurement = {}
    
    constructor() {
        this.performance = performance || window.performance
    }

    start(...keys) {
        for (const key of keys) {
            this.measurement[key] = { start: this.performance.now(), stop: null }
        }
    }

    stop(...keys) {
        for (const key of keys) {
            this.measurement[key].stop = this.performance.now()
        }
    }

    log(...keys) {
        for (const key of keys) {
            const { start, stop } = this.measurement[key]
            console.log(`${key} took ${stop - start} ms`);
        }
    }
    logAll() {
        this.log(...Object.keys(this.measurement))
    }
}