import PerformanceLogger from './../src/performanceLogger'

describe('PerformanceLogger', () => {
    test('start(), stop() to have values', async () => {
        // Arrange
        const waitFor = async (t) => new Promise((resolve) => setTimeout(() => resolve(t), t));
        const logger = new PerformanceLogger()

        // Act
        logger.start('A', 'B')
        await waitFor(1)
        logger.stop('A', 'B')
        logger.logAll()

        // Assert
        expect(logger.measurement.A.stop).toBeGreaterThan(1)
        expect(logger.measurement.B.stop).toBeGreaterThan(1)
    })

})